# What is SUID-HELPER?

It is a small Ada library that helps in writing safer suid  programs (that is, programs that have the set-uid bit set and run with the privilege of the program owner).

A well-known good practice in writing suid programs is to drop the owner's privileges and restore them only when necessar; in this way it is reduced the possibility that a bug in the code could give rise to a security problem.  Dropping correctly the privileges it is not difficult, but [a bit tricky](https://www.oreilly.com/library/view/secure-programming-cookbook/0596003943/ch01s03.html).Library `SUID-HELPER` helps doing it correctly.

## System requirements

I developed this on Linux, but it should work on more or less any *nix-like system.  The idea of set-uid bit and the way permissions are managed it is very *nix-like and I do not know if it would even make sense to port this to other systems.  If you think that something similar could be used also in Windows, let me know and we can try to extend its applicability.

The Ada compiler I used is GNAT.  I do not think that there will be any portability problems to other compilers, but you can need differente project files.  

## How do I install it?

Just put the source code where your compiler will  find it and `with` the project inside yours.

## How do I use it?

This file gives you a brief introduction.  As usual, comments in suid-helper.ads file are a more detailed documentation. 

# How does it help be in writing suid code?

The package `suid_helper` has an initialization part that drops the privileges and any [ancillary groups](https://www.oreilly.com/library/view/secure-programming-cookbook/0596003943/ch01s03.html) during the initial elaboration, so that when the main procedure starts the privileges are down.

The only way to restore the privileges is via the procedure `With_Privileges_Do` that exists in three variants.  Let's see first the simplest variant

```Ada
procedure With_Privileges_Do (Callback         : access procedure;
                              Drop_Permanently : Boolean := True);
```
This procedure expects as parameter a pointer to a parameterized procedure.  The semantic is really simple: the privileges are restored, `Callback` called and the privileged are dropped again.  Unless the second parameter is false, the privileges are permanently dropped and it will not be possible to restore them again.  

This is a typical usage example whhere the callback is defined _locally_ inside a `declare` block
```Ada
   declare
      procedure Unmount_Callback is
      begin
         Unmount (Mountpoint_Name);
      end Unmount_Callback;
   begin
      With_Privileges_Do (Callback         => Unmount_Callback'Access,
                          Drop_Permanently => True);
   end;
```

## Using a handler object

The use of a callback has the advantage of simplicity, but it does not allow for passing parameters to the callback.  A solution is shown above: the callback can be  defined locally in order to have the visibility of the necessary values.  An alternative solution is the use of a handler according to 
```Ada
   type Privileged_Object is interface;

   procedure Call (Callback : Privileged_Object)
   is abstract;
   
   procedure With_Privileges_Do (Callback         : Privileged_Object'Class;
                                 Drop_Permanently : Boolean := True);
 

```

In this case the procedure `With_Privileges_Do` expects as parameter an object implementing interface `Privileged_Object` which just requires a method `Call`.  The parameters to the callback can be passed as the state of the object. 

The following is a (fictional and partial) example of usage
```Ada
   type User_Handler is new Privileged_Object with private;
   
   overriding procedure Call (Callback : User_handler);

   function Usr_handler(Useraname : string; Password : string) return User_Handler;
   
   --------------------------------------------
   
   With_Privilege_Do(Callback         => Usr_Handler("foo", "pa$$w0rd"), 
                     Drop_Permanently => False);
```

## Returning values

If we wanto to get values back from the callback, they can be stored in the status visible to the callback or maybe an access can be given to the handler (note that `Callback` has `in` mode, making it read-only; I decided in this way to allow for the call of the constructur directly in the parameter list).  A further solution is to use the third flavor that allows for a return value

```Ada
   generic 
      type Callback_Result (<>) is limited private;
   function With_Privileges_Call_Function
     (Callback         : access function return Callback_Result;
      Drop_Permanently : Boolean := True)
      return Callback_Result;
      
```
In this case the function is a generic parameterized by the type of the return value.  The expected parameter is a parameterless function that returns a  `Callback_Result`.

## Environment variables 

Since enviromental variables can be a source of security holes, the package during the elaboration phase saves the the environment and then it cleans it.
More precisely, the package mantains three "enviroments"

1. The _tainted_ environment.  This is a copy of the original environment. It cannot be changed, but it can be read with `Tainted_Variable`
2. The _user_ environment.  This is used when the privileges are dropped. It is initially empty and it can be written with `Set_User_Environment`.
3. The _safe_ environment used when privileges are on. It is initially empty and it can be written with `Set_Safe_Environment` and `Bless`.
