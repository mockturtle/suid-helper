pragma Ada_2012;
with Gnat.OS_Lib;  use Gnat.OS_Lib;

package body Suid.OS.POSIX_Thick is
   use Interfaces;
   use type Interfaces.C.Int;

   function Getgroups (Size : C.Int; Data : access Group_ID) return C.Int
         with
               Import => True,
               Convention => C,
               External_Name => "getgroups";

   function Setgroups (Size : C.Int; Data : access  constant Group_ID) return C.Int
         with
               Import => True,
               Convention => C,
               External_Name => "setgroups";

   function Set_All_User_Ids (Real_ID      : User_ID;
                              Effective_Id : User_ID;
                              Saved_Id     : User_ID)
                              return C.Int
         with
               Import => True,
               Convention => C,
               External_Name => "setresuid";

   function Set_All_Group_Ids (Real_ID      : Group_ID;
                               Effective_Id : Group_ID;
                               Saved_Id     : Group_ID)
                               return C.Int
         with
               Import => True,
               Convention => C,
               External_Name => "setresuid";

   ---------------------------
   -- Set_Effective_User_Id --
   ---------------------------

   procedure Set_Effective_User_Id (Id : User_ID) is
      function Seteuid (New_ID : User_ID) return Interfaces.C.Int
            with Import, Convention => C;
   begin
      if Seteuid (Id) < 0 then
         raise Constraint_Error with Errno_Message;
      end if;
   end Set_Effective_User_Id;

   ----------------------------
   -- Set_Effective_Group_Id --
   ----------------------------

   procedure Set_Effective_Group_Id (Id : Group_ID) is
      function Setegid (New_ID : Group_ID) return Interfaces.C.Int
            with Import, Convention => C;
   begin
      if Setegid (Id) < 0 then
         raise Constraint_Error with Errno_Message;
      end if;
   end Set_Effective_Group_Id;

   function Get_Ancillary_Groups return Group_Array
   is
      Empty    : Group_Array (2 .. 1);
      N_Values : constant C.Int := Getgroups (0, null);
   begin
      if N_Values = 0 then
         return Empty;
      else
         declare
            Result : aliased Group_Array (1 .. Positive (N_Values));
            Err    : C.Int;
         begin
            Err := Getgroups (N_Values, Result (1)'Access);

            if Err < 0 then
               raise Constraint_Error with Errno_Message;
            end if;

            return Result;
         end;
      end if;
   end Get_Ancillary_Groups;

   procedure Clear_Ancillary_Groups
   is
   begin
      if Setgroups (0, null) < 0 then
         raise Constraint_Error with Errno_Message;
      end if;
   end Clear_Ancillary_Groups;

   procedure Set_Ancillary_Groups (Groups : Group_Array)
   is
   begin
      if Setgroups (Groups'Length, Groups (Groups'First)'Access) < 0 then
         raise Constraint_Error with Errno_Message;
      end if;
   end Set_Ancillary_Groups;

   procedure Set_All_User_Ids (ID : User_ID)
   is
   begin
      if Set_All_User_Ids (Id, Id, Id) < 0 then
         raise Constraint_Error with Errno_Message;
      end if;
   end Set_All_User_Ids;

   procedure Set_All_Group_Ids (ID : Group_ID)
   is
   begin
      if Set_All_Group_Ids (Id, Id, Id) < 0 then
         raise Constraint_Error with Errno_Message;
      end if;
   end Set_All_Group_Ids;


end Suid.OS.POSIX_Thick;
