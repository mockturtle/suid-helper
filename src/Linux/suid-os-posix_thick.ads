with Interfaces.C;
--
-- This package provides interfaces with the API of the OS, providing
-- a more Ada-esque interface
--
package Suid.OS.POSIX_Thick is

   type User_ID is private;
   type Group_ID is private;
   type Group_Array is array (Positive range <>) of aliased Group_ID;

   Root_ID : constant User_ID;

   function Image (Id : User_ID) return String;
   function Image (Id : Group_ID) return String;

   function Get_Effective_User_Id return User_ID
         with Import,
         Convention => C,
         External_Name => "geteuid";

   function Get_Effective_Group_ID return Group_ID
         with  Import,
         Convention => C,
         External_Name => "getegid";

   function Get_User_Id return User_ID
         with Import, Convention => C, External_Name => "getuid";

   function Get_Group_Id return Group_ID
         with Import, Convention => C, External_Name => "getgid";

   procedure Set_Effective_User_Id (Id : User_ID);

   procedure Set_Effective_Group_Id (Id : Group_ID);

   function Get_Ancillary_Groups return Group_Array;

   procedure Clear_Ancillary_Groups;

   procedure Set_Ancillary_Groups (Groups : Group_Array);

   procedure Set_All_User_Ids (ID : User_ID);

   procedure Set_All_Group_Ids (ID : Group_ID);

   function Am_I_Root return Boolean
   is (Get_User_Id = Root_ID);


private
   type User_ID is new Interfaces.C.Unsigned;
   type Group_ID is new Interfaces.C.Unsigned;

   Root_ID : constant User_ID := 0;


   function Image (Id : User_ID) return String
   is (User_ID'Image (Id));

   function Image (Id : Group_ID) return String
   is (Group_ID'Image (Id));
end Suid.OS.POSIX_Thick;
