with Suid.Os.POSIX_Thick;

--
-- Implementation of the Suid.OS services for a POSIX-like system
--

--
-- Changing privileges in a safe way is a delicate matter. See
--
-- [1] https://wiki.sei.cmu.edu/confluence/display/c/POS36-C.+Observe+correct+revocation+order+while+relinquishing+privileges
-- [2] https://www.oreilly.com/library/view/secure-programming-cookbook/0596003943/ch01s03.html
--
-- See also the paper "Setuid Demystified"
--
-- [3] https://people.eecs.berkeley.edu/~daw/papers/setuid-usenix02.pdf
--
-- and this follow-up
--
-- [4] https://www.usenix.org/system/files/login/articles/325-tsafrir.pdf
--

package body Suid.Os is
   Saved_Effective_User_ID  : constant POSIX_Thick.User_ID := POSIX_Thick.Get_Effective_User_Id;
   Saved_Effective_Group_ID : constant POSIX_Thick.Group_ID := POSIX_Thick.Get_Effective_Group_ID;
   Saved_Ancillary_Groups   : constant POSIX_Thick.Group_Array := POSIX_Thick.Get_Ancillary_Groups;


   procedure Drop_Privileges (Drop_Permanently : Boolean)
   is
   begin
      --
      -- NOTE: As first step drop any ancillary group, according
      -- to recommendation [4] cited above.
      --
      POSIX_Thick.Clear_Ancillary_Groups;

      --
      -- NOTE: Drop first the group id and then the user id according
      -- to recommendations [1-4] cited above
      --
      if Drop_Permanently then
         POSIX_Thick.Set_All_Group_Ids (POSIX_Thick.Get_Group_Id);
         POSIX_Thick.Set_All_User_IDs (POSIX_Thick.Get_User_Id);
      else
         POSIX_Thick.Set_Effective_Group_Id (POSIX_Thick.Get_Group_Id);
         POSIX_Thick.Set_Effective_User_Id (POSIX_Thick.Get_User_Id);
      end if;

   end Drop_Privileges;

   procedure Restore_Privileges is
   begin
      POSIX_Thick.Set_Effective_User_Id (Saved_Effective_User_ID);
      POSIX_Thick.Set_Effective_Group_Id (Saved_Effective_Group_ID);
      POSIX_Thick.Set_Ancillary_Groups (Saved_Ancillary_Groups);
   end Restore_Privileges;
end Suid.Os;
