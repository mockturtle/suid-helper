pragma Ada_2012;
with Suid.OS;
with Ada.Environment_Variables;

with Ada.Containers.Indefinite_Ordered_Maps;


package body Suid.Helper is
   type Privilege_Status is (Active, Dropped, Permanently_Dropped);

   Current_Privilege_Status : Privilege_Status := Active;

   package String_Maps is
     new Ada.Containers.Indefinite_Ordered_Maps (Key_Type     => String,
                                                 Element_Type => String);

   subtype Environment_Type is String_Maps.Map;


   Original_Environment : Environment_Type := String_Maps.Empty_Map;
   User_Environment     : Environment_Type := String_Maps.Empty_Map;
   Safe_Environment     : Environment_Type := String_Maps.Empty_Map;

   ----------------------
   -- Save_Environment --
   ----------------------

   procedure Save_Environment (To : in out Environment_Type)
   is
      ----------
      -- Save --
      ----------

      procedure Save (Name, Value : String) is
      begin
         To.Insert (Key      => Name,
                    New_Item => Value);
      end Save;
   begin
      Ada.Environment_Variables.Iterate (Save'Access);
   end Save_Environment;

   ---------------------
   -- Set_Environment --
   ---------------------

   procedure Set_Environment (Env : Environment_Type) is
      use String_Maps;
   begin
      Ada.Environment_Variables.Clear;
      for Pos in Env.Iterate loop
         Ada.Environment_Variables.Set (Name  => Key (Pos),
                                        Value => Element (Pos));
      end loop;
   end Set_Environment;

   --------------------------
   -- Set_Safe_Environment --
   --------------------------

   procedure Set_Safe_Environment (Name : String;  Value : Safe_Value)
   is
      use Ada.Strings.Unbounded;
   begin
      Safe_Environment.Include (Key      => Name,
                                New_Item => To_String (Value));

      if Current_Privilege_Status = Active then
         Set_Environment (Safe_Environment);
      end if;
   end Set_Safe_Environment;

   --------------------------
   -- Set_User_Environment --
   --------------------------

   procedure Set_User_Environment (Name, Value : String)
   is
   begin
      User_Environment.Include (Key      => Name,
                                New_Item => Value);


      if Current_Privilege_Status /= Active then
         Set_Environment (User_Environment);
      end if;
   end Set_User_Environment;


   function Is_Restore_Possible return Boolean
   is (Current_Privilege_Status /= Permanently_Dropped);

   procedure Drop_Privileges (Drop_Permanently : Boolean)
     with Post => (Drop_Permanently = not Is_Restore_Possible);


   procedure Restore_Privileges
     with Pre => Is_Restore_Possible;

   procedure Drop_Privileges (Drop_Permanently : Boolean)
   is
   begin
      if Current_Privilege_Status /= Active then
         return;
      end if;


      Os.Drop_Privileges (Drop_Permanently);

      Current_Privilege_Status :=
        (if Drop_Permanently then Permanently_Dropped else Dropped);

      Set_Environment (User_Environment);
   end Drop_Privileges;

   procedure Restore_Privileges
   is
   begin
      if Current_Privilege_Status = Permanently_Dropped then
         raise Restore_Impossible;
      end if;

      Set_Environment (Safe_Environment);

      Os.Restore_Privileges;

      Current_Privilege_Status := Active;
   end Restore_Privileges;

   ------------------------
   -- With_Privileges_Do --
   ------------------------

   procedure With_Privileges_Do (Callback         : in out Privileged_Object'Class;
                                 Drop_Permanently : Boolean := True)
   is
   begin
      Restore_Privileges;

      Callback.Call;

      Drop_Privileges (Drop_Permanently);
   exception
      when others =>
         Drop_Privileges (True);
         raise;
   end With_Privileges_Do;

   ------------------------
   -- With_Privileges_Do --
   ------------------------

   procedure With_Privileges_Do (Callback         : access procedure;
                                 Drop_Permanently : Boolean := True)
   is
   begin
      Restore_Privileges;

      Callback.all;

      Drop_Privileges (Drop_Permanently);
   exception
      when others =>
         Drop_Privileges (True);
         raise;
   end With_Privileges_Do;

   ------------------------
   -- With_Privileges_Do --
   ------------------------

   function With_Privileges_Call_Function
     (Callback         : access function return Callback_Result;
      Drop_Permanently : Boolean := True)
      return Callback_Result
   is
   begin
      Restore_Privileges;

      return Result : Callback_Result := Callback.all do
         Drop_Privileges (Drop_Permanently);
      end return;
   exception
      when others =>
         Drop_Privileges (True);
         raise;
   end With_Privileges_Call_Function;

   function Exist_Tainted (Name : String) return Boolean
   is (Original_Environment.Contains (Name));

   function Tainted_Variable (Name : String; Default : String) return String
   is (if Exist_Tainted (Name) then
          Original_Environment.Element (Name)
       else
          Default);

   function Tainted_Variable (Name : String) return String
   is (if Exist_Tainted (Name) then
          Original_Environment.Element (Name)
       else
          raise Constraint_Error);



begin
   Save_Environment (Original_Environment);
   Save_Environment (User_Environment);
   Set_Environment (Safe_Environment);
   Drop_Privileges (False);
end Suid.Helper;
