with Ada.Strings.Unbounded;
--
-- It is considered good practice in set-uid programs to drop the privileges
-- when they are not needed and restore effective uid privileges only when
-- necessary.  This small library helps in following this good practice.
--
-- During the elaboration phase this package drops the privileges, so
-- that when the program starts the privileges are not active.
--
-- The only way to restore the privileges is via the procedure
-- With_Privileges_Do that expects as parameter a
-- "handler" that is called with privileges on.
-- When the handler returns, the privileges are dropped again.  Unless
-- specified otherwise, the privileged by default are permanently dropped
-- any tentative of calling With_Privileges_Do again will raise an exception.
--
-- With_Privileges_Do is provided in three versions that differ in the type
-- of handler
--
-- 1. In the simplest case the handler is just a parameterless procedure
--    that is called
--
-- 2. If a return value is desired, there is a generic version to be
--    instantiated with the return type and that expects as
--    parameter a parameterless function.
--
-- 3. In a more general case the With_Privileges_Do expects a "handler
--    object" descendent of Privileged_Object and that implements the
--    procedure Call.
--
-- In every case the procedure expects a second optional parameter
-- Drop_Permanently that is True by default.  If Drop_Permanently is True,
-- after the handler returns, the privileges are permanently dropped.
--
-- ## Environmental variables
--
-- Since enviromental variables can be a source of security holes, the
-- package during the elaboration phase saves the the environment
-- and then it cleans it.
--
-- More precisely, the package mantains three "enviroments"
--
-- 1. The _tainted_ environment.  This is a copy of the original environment.
--    It cannot be changed, but it can be read with Tainted_Variable
--
-- 2. The _user_ environment.  This is used when the privileges are dropped.
--    It is initially empty and it can be written with Set_User_Environment.
--
-- 3. The _safe_ environment used when privileges are on.
--    It is initially empty and it can be written with Set_Safe_Environment.
--

package Suid.Helper is
   --
   -- Return false if the privileges have been permanently dropped.
   -- Useful in pre/post conditions
   --
   function Is_Restore_Possible return Boolean;

   --
   -- Simplest case: the handler is a parameterless procedure
   --
   procedure With_Privileges_Do (Callback         : access procedure;
                                 Drop_Permanently : Boolean := True)
         with
               Pre => Is_Restore_Possible,
               Post => (Drop_Permanently = not Is_Restore_Possible);

   --
   -- Generic definition when a return value is desired
   --
   pragma Warnings (Off, "postcondition does not mention function result");
   generic
      type Callback_Result (<>) is limited private;
   function With_Privileges_Call_Function
     (Callback         : access function return Callback_Result;
      Drop_Permanently : Boolean := True)
      return Callback_Result
         with
               Pre => Is_Restore_Possible,
               Post => (Drop_Permanently = not Is_Restore_Possible);

   --
   -- Most general case where the handler is an object descending from
   -- the interface Privileged_Object
   --
   type Privileged_Object is interface;

   procedure Call (Callback : in out Privileged_Object)
   is abstract;


   procedure With_Privileges_Do (Callback         : in out Privileged_Object'Class;
                                 Drop_Permanently : Boolean := True)
         with
               Pre => Is_Restore_Possible,
       Post => (Drop_Permanently = not Is_Restore_Possible);

   -- Raised when calling With_Privileges_Do with the privileges
   -- permanently dropped
   Restore_Impossible : exception;

   --
   -- Only so-called "safe values" can be stored in the safe environment.
   -- A string must be declared safe with the function Bless.  A Safe_Value
   -- it is just an Unbounded_String, so it has no intrinsic safety;
   -- it is just a reminder that values read from the original environment
   -- need to be considered unsafe and sanitized.
   --
   -- Is there something that prevent you to just Bless a tainted
   -- value? No, there isn't.  I have no idea about how to force
   -- you doing this.
   --
   type Safe_Value is private;

   function Bless (Item : String) return Safe_Value;

   --
   -- Read the value of a variable in the original environment.
   -- The name "Tainted_Variable" is to remember that those values
   -- are not to be trusted as they are.  The behavior is like the
   -- function Value in Ada.Environmental_Variables
   --
   function Tainted_Variable (Name : String) return String
     with
       Pre => Exist_Tainted (Name),
     Post => (if not Exist_Tainted (Name) then raise Constraint_Error);


   function Tainted_Variable (Name : String; Default : String) return String
     with
       Post => (if not Exist_Tainted (Name) then Tainted_Variable'Result = Default);

   --
   -- Return True if a variable with the specified variable exists
   -- in the saved environment
   --
   function Exist_Tainted (Name : String) return Boolean;



   --
   -- Write a variable in the safe environment (used when privileges are on)
   --
   procedure Set_Safe_Environment (Name : String;  Value : Safe_Value);

   --
   -- Write a variable in the user environment (used when privileges are off)
   --
   procedure Set_User_Environment (Name, Value : String);
private
   type Safe_Value is new Ada.Strings.Unbounded.Unbounded_String;

   function Bless (Item : String) return Safe_Value
   is (Safe_Value (Ada.Strings.Unbounded.To_Unbounded_String (Item)));
end Suid.Helper;
