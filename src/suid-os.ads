--
-- This package provides two functions to drop and restore privileges.
-- Those actions require to call OS specific function. This package
-- presents an interface that is independent on the specific OS,
-- hiding the OS details in the body.
--
-- Porting this library to another OS requires only to change the body.
--
private package Suid.Os is
   --
   -- Drop privileges, if Drop_Permanently is True carry out any action
   -- that makes privilege restoring impossibile, if the OS allows that.
   -- Note that the Drop_Permanently could be not honored for some OSes.
   --
   procedure Drop_Privileges (Drop_Permanently : Boolean);

   --
   -- Restore the privileges and run the software in privileged mode.
   --
   procedure Restore_Privileges;
end Suid.Os;
