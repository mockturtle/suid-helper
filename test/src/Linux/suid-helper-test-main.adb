procedure Suid.Helper.Test.Main is
   -- Just call the procedure defined in the parent package.
   -- Why this funny solution?  Because we want to be able to access
   -- the private package Suid_Helper.OS and the simplest solution is
   -- having the actual main as child of a private sibling of Suid_Helper.OS
begin
   Do_Test;
end Suid.Helper.Test.Main;
