with Ada.Text_IO; use Ada.Text_IO;
with Ada.Tags;
with Ada.Environment_Variables;

--
-- We need to "with" this low-level package since we need it to get
-- user ID, group ID, and so on...
--
with Suid.OS.POSIX_Thick;

package body Suid.Helper.Test is

   use Os.POSIX_Thick;

   function With_Privileges_Integer is
     new With_Privileges_Call_Function (Integer);

   function Callback_With_Result return Integer
   is (120);


   procedure Call (X : in out Test_Handler)
   is
   begin
      Put_Line ("Handler : " & Integer'Image (X.N));
   end Call;

   function Create (A : Integer) return Test_Handler
   is (Test_Handler'(N => A));


   procedure Do_Test is

      procedure Print (X : Group_Array) is
      begin
         Put_Line ("<<<");
         for G of X loop
            Put_Line ("  " & Image (G));
         end loop;
         Put_Line (">>>");
         New_Line;
      end Print;

      procedure Callback is
      begin
         Print (Get_Ancillary_Groups);
         Put_Line ("Effective user  : " & Image (Get_Effective_User_Id));
         Put_Line ("Real User       : " & Image (Get_User_Id));

         Put_Line ("Effective group : " & Image (Get_Effective_Group_ID));
         Put_Line ("Real Group      : " & Image (Get_Group_ID));

         Put_Line ("PATH : " & Ada.Environment_Variables.Value ("PATH", "NULL"));
         Put_Line ("OLDSTUFF : " & Ada.Environment_Variables.Value ("OLDSTUFF", "NULL"));
      end Callback;

      Raised : Boolean;
   begin
      -- This should show real id=effective id since privileges here
      -- are dropped
      Callback;

      Set_User_Environment ("PATH", "User path");
      Set_User_Environment ("OLDSTUFF", Tainted_Variable ("PATH"));
      Set_Safe_Environment ("PATH", Bless ("safe path"));

      Callback;

      -- This should show real /= effective since inside the callback
      -- the privileges are restored
      With_Privileges_Do (Callback'Access, False);

      -- Test the callback with return value
      Put_Line
        ("Result : 120="
         & Integer'Image
           (With_Privileges_Integer (Callback_With_Result'Access, False)));

      -- Test the object version and drop permanently the privileges
      declare
         Handler : Test_Handler := Create (42);
      begin
         With_Privileges_Do (Handler);
      end;

      begin
         Raised := False;
         With_Privileges_Do (Callback'Access);
      exception
         when Restore_Impossible =>
            Raised := True;
      end;

      if Raised then
         Put_Line ("Exception raised as expected");
      else
         Put_Line ("Exception NOT raised, why??");
      end if;
   end Do_Test;
end Suid.Helper.Test;
