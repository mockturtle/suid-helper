private
package Suid.Helper.Test is
   type Test_Handler is new Privileged_Object with
      record
         N : Integer;
      end record;

   overriding procedure Call (X : in out Test_Handler);

   function Create (A : Integer) return Test_Handler;

   -- Actually test procedure.  See comments in Suid_Helper.Test.Main
   procedure Do_Test;
end Suid.Helper.Test;
